{
  "username": "PinkSquad",
  "avatar_url": "https://cdn.discordapp.com/attachments/748276700143681687/786675271964164186/a74c36296a34bf07.png",
  "content": " ",
  "embeds": [
    {
      "title": "<a:warn:794866603623841824>**Запрещена игра с читами и другими вспомогательными программами. **",
      "color": 16711680,
      "description": "За нечестную игру вам будет выдана роль <@&768502575582150728> \nПри первом предупреждении вам будет выдана эта роль на **неделю**, при втором -**две недели**, а при третьем - **бан** на категорию \"Буткемп\".\nТак же хочется заметить, что люди получившие эту роль, лишены возможности участвовать в **составе управления на 4 месяца.**\nЭта роль не ограничивает вас в правах, но вы будете выделяться среди других пользователей, и люди будут знать, что **Вы - нечестный игрок. **\n```diff\n- Варн/бан на категорию```\n",
      "timestamp": "",
      "author": {},
      "image": {
        "url": "https://images-ext-1.discordapp.net/external/8XxpCyxeUPOdgCOIZuWlYzsdA-c8ki1SszLiO6n8nLE/https/images-ext-2.discordapp.net/external/7DeytwE35SQOKeusufHE0PbIXl9HPVthwv2mNiqPIug/https/cdn.probot.io/3MPjP6UgPz.gif"
      },
      "thumbnail": {
        "url": "https://images-ext-1.discordapp.net/external/M-AkuZ7Im6vbf7sxEoqCYlzRTu3bmmZeBa3FGW_7VRk/https/media.discordapp.net/attachments/748276700143681687/779458140437217280/O7BZ.gif"
      },
      "footer": {},
      "fields": []
    },
    {
      "title": "<a:warn:794866603623841824>**Игровые роли**<a:warn:794866603623841824>",
      "color": 16711680,
      "description": "**Нажав на реакцию, вы получаете соответствующую ей игровую роль, нажав повторно, вы её снимите.**\n\n<a:r_:786934293799763988><:au_AmongUs:761870534157533204> - Among Us\n<a:r_:786934293799763988><:game_Dota2:763836382124179516>- Dota2\n<a:r_:786934293799763988><:game_osu:763836447694127104> - osu!\n<a:r_:786934293799763988><:game_lol:763836367317762140> - League of Legends\n<a:r_:786934293799763988><:game_mobile_legends:763850093308411964>- Mobile Legends\n<a:r_:786934293799763988><:game_CSGO:761871881216327700>- Counter-Strike: Global Offensive\n<a:r_:786934293799763988><:game_call_of_duty_mobile:763842149082464256> - Call of Duty: Mobile\n<a:r_:786934293799763988><:game_rainbow_six_siege:763836459870060554> - Tom Clancy's Rainbow Six Siege\n<a:r_:786934293799763988><:game_pubg:761876770914828298> - PUBG MOBILE\n<a:r_:786934293799763988><:game_valorant:763840146579521546> - Valorant\n<a:r_:786934293799763988><:game_minecraft:763836868453859369> - Minecraft\n<a:r_:786934293799763988><:game_Fornite:763844500111622164>- Fortnite\n<a:r_:786934293799763988><:game_wot:763850113294401616> - World of Tanks\n<a:r_:786934293799763988><:game_wotblitzmod:763903644621733918> - World of Tanks Blitz\n<a:r_:786934293799763988><:game_genshin_impact:766337646116864010>- Genshin Impact\n<a:r_:786934293799763988><:game_gta5:763840783991308308> - GTA 5",
      "timestamp": "",
      "author": {},
      "image": {
        "url": "https://images-ext-1.discordapp.net/external/8XxpCyxeUPOdgCOIZuWlYzsdA-c8ki1SszLiO6n8nLE/https/images-ext-2.discordapp.net/external/7DeytwE35SQOKeusufHE0PbIXl9HPVthwv2mNiqPIug/https/cdn.probot.io/3MPjP6UgPz.gif"
      },
      "thumbnail": {
        "url": "https://images-ext-1.discordapp.net/external/vzkDsQCxzVV3uGE8nRgxBkhkvMOm4uA-A9ogEOrmK2o/https/media.discordapp.net/attachments/748276700143681687/779457309323558922/1fqv.gif"
      },
      "footer": {},
      "fields": []
    }
  ]
}