{
  "username": "PinkSquad",
  "avatar_url": "https://cdn.discordapp.com/attachments/748276700143681687/786675271964164186/a74c36296a34bf07.png",
  "content": " ",
  "embeds": [
    {
      "color": 16711935,
      "timestamp": "",
      "author": {},
      "image": {
        "url": "https://media.discordapp.net/attachments/748276700143681687/763800968117878794/download_93.gif"
      },
      "thumbnail": {},
      "footer": {},
      "fields": []
    },
    {
      "title": "<a:firepink:694423413858762792>Система верификации<a:firepink:694423413858762792>",
      "color": 16711935,
      "description": "**Привет, солнце!** Ты находишься на сервере **PinkSquad**! Первым делом прочитай <#741763992917377095>, а после можешь нажать на реакцию <a:some_galka:766779737263046687> , чтобы получить полный доступ к серверу.\nЕсли у тебя возникли проблемы, ты можете упомянуть роль <@&741809242427621409> и <@&742513295596191838> в чате <#766750787740762152> или зайти в доступный тебе голосовой канал с названием \"Ожидание\". Приятного тебе общения!",
      "timestamp": "",
      "author": {},
      "image": {
        "url": "https://images-ext-2.discordapp.net/external/7DeytwE35SQOKeusufHE0PbIXl9HPVthwv2mNiqPIug/https/cdn.probot.io/3MPjP6UgPz.gif"
      },
      "thumbnail": {},
      "footer": {},
      "fields": []
    }
  ]
}
